var express = require('express')
var app = express()
var chokidar = require('chokidar')
var watcher = chokidar.watch('./app')
var bodyParser = require('body-parser')


app.use(bodyParser.json())

const port = process.env.PORT || 3000;

const server = app.listen(port);

server.timeout = 1000 * 60 * 10; // 10 minutes

watcher.on('ready', function() {
  watcher.on('all', function() {
    console.log("Clearing /dist/ module cache from server")
    Object.keys(require.cache).forEach(function(id) {
      if (/[\/\\]app[\/\\]/.test(id)) delete require.cache[id]
    })
  })
})
app.use(function (req, res, next) {
  require('./app')(req, res, next)
})

