
const datosCliente = {
    "status": "OK",
    "fault": null,
    "data": {
      "identificacionCliente": null,
      "cliente": {
        "personaNatural": {
          "nombreLargoCliente": null,
          "nombreCortoCliente": null,
          "nombreCliente": {
            "primerNombre": "FERNEY",
            "segundoNombre": "ABELARDO",
            "primerApellido": "MARULANDA",
            "segundoApellido": ""
          }
        },
        "personaJuridica": null,
        "tipoPersona": null
      },
      "identificadorClienteBP": null,
      "llaveMDM": null,
      "llaveCIF": null,
      "codigoCIIU": "00010",
      "codigoSubCIIU": null,
      "codigoSegmento": "1",
      "codigoSubSegmento": null,
      "gerenteMonedaExtranjera": null,
      "gerenteComercial": {
        "nombreLargo": "Carlos Alberto Valderrama Palacios",
        "codigo": "EMP001"
      },
      "codigoRol": "02",
      "estadoCliente": null,
      "estadoVinculacion": "00",
      "fechaEstadoVinculacion": null,
      "fechaCambioEstado": null,
      "codigoRegion": null,
      "codigoOficina": "BTA001",
      "codigoSector": null,
      "codigoSubSector": null,
      "informacionExpedicionIdentificacion": {
        "fechaExpedicion": "1996-08-21T00:00:00.000+0000",
        "codigoCiudadExpedicion": "82",
        "codigoPaisExpedicion": "CO"
      },
      "informacionNacimientoCliente": null,
      "ocupacion": "1",
      "esIntermediarioMercadoCambiario": null,
      "esVigiladoPorSuperfinanciera": null,
      "fechaUltimaActualizacion": "2019-03-14T00:00:00.000+0000",
      "esClienteDeTesoreria": null,
      "usuarioRedTrader": null,
      "codigoGrupoPersona": "2"
    }
};

const datosUbicacion = {
  "status": "OK",
  "data": {
    "informacionUbicacionCliente": [
      {
        "identificacionCliente": null,
        "codigoDireccionFuente": "0069421916",
        "codigoTipoDireccionFuente": "Z002",
        "direccionPrincipal": "LABORAL 1",
        "codigoCiudad": "8001000",
        "codigoDepartamento": "08",
        "codigoPais": null,
        "telefonoFijo": "4556677",
        "celular": "3015659286",
        "fax": null,
        "correoElectronico": "cguzmanp@co.ibm.com",
        "paginaWeb": null,
        "codigoPostal": "0008001000",
        "barrioDireccionPrincipal": "Pescaito",
        "codigoCorrespondencia": null,
        "llaveDireccionCif": null
      }
    ]
  }
};

const errores = {
    "errores": [
        {
            "servicio": "GestionarProspecto",
            "operacion": "modificarPersonaNatural",
            "codigoError": "2",
            "tipoExcepcion": "crmBusinessException",
            "descripcionInterna": "Prospecto/cliente ya existe en CRM con el mismo núm. de documento Externo",
            "mensajeFuncional": [
                {
                    "codigoFuncional": "GPMPN2",
                    "descripcionFuncional": "GPMPN2|¡Espera! No puedes terminar el proceso por este medio. Para finalizarlo, acércate a una de nuestras oficinas."
                }
            ]
        },
        {
            "servicio": "GestionarProspecto",
            "operacion": "modificarPersonaNatural",
            "codigoError": "3",
            "tipoExcepcion": "crmBusinessException",
            "descripcionInterna": "Prospecto/cliente no existe en CRM con el tipo/número de documento",
            "mensajeFuncional": [
                {
                    "codigoFuncional": "GPMPN3",
                    "descripcionFuncional": "GPMPN3|¡Espera! No puedes terminar el proceso por este medio. Para finalizarlo, acércate a una de nuestras oficinas."
                }
            ]
        }
    ]
};


const fault = {
    "status": "KO",
    "fault": {
          "faultType":"SystemException",
          "faultCode":"400",
          "faultDescription":"Error xxx",
          "stackTrace":"stack defecto"
      },
    "data": null
};

const listasControl = {
    "status": "OK",
    "fault": null,
    "data": {
      "codigoRespuesta": "200",
      "descripcionRespuesta": "LA RESPUESTA DEL SERVICIO HA SIDO EXITOSA"
    }
};

const authFuerte = {
  "status": "200 OK",
  "fault": null,
  "data": {
    "identificacionCliente": {
      "tipoDocumento": "CC",
      "numeroDocumento": "1234568912"
    },
    "mecanismo": "Web",
    "correoElectronico": "email@email.com",
    "numeroCelular": "3219874582",
    "fechaInscripcion": "15-03-2019",
    "ultimaFechaModificacionDatos": "15-03-2019",
    "ultimaFechaModificacionMecanismo": "15-03-2019",
    "estadoServicioOTP": "Done",
    "metodoEnvioOTPODA": "Fisica"
  }
};

const tipoCliente = {
  "resDatosUbicacion": {
    "data": {
      "informacionUbicacionCliente": [
        {
          "codigoCorrespondencia": "X",
          "identificacionCliente": {
            "tipoDocumento": "FS001",
            "numeroDocumento": "25130542"
          },
          "codigoDepartamento": "17",
          "barrioDireccionPrincipal": "               ",
          "codigoTipoDireccionFuente": "Z001",
          "direccionPrincipal": "CL 21 8A 23",
          "codigoCiudad": "17001000",
          "llaveDireccionCif": "190301082428003",
          "codigoPais": {
            "codigoISO3166Alfa2": "CO"
          },
          "telefonoFijo": "8483478"
        }
      ]
    },
    "status": "OK"
  },
  "resTipoCliente": {
    "response": {
      "idAplicacion": "otrosPlanes",
      "idSesion": "369574501169-mF2YS8xLjguMF8366318670576",
      "urlRedirect": "https://autenticaqa3.bancolombia.com/login/oauth/authorize?response_type=code&client_id=YCL&redirect_uri=https://qaabrircuenta.grupobancolombia.com/bancolombia.dtd.b1c.yaCliente/home"
    },
    "status": 200
  },
  "resDatosAuthFuerte": {
    "data": {
      "numeroCelular": "3147061764",
      "identificacionCliente": {
        "tipoDocumento": "FS001",
        "numeroDocumento": "25130542"
      },
      "ultimaFechaModificacionDatos": "10-04-2019",
      "metodoEnvioOTPODA": "EML",
      "estadoServicioOTP": "0000",
      "mecanismo": "ODA",
      "correoElectronico": "maourib@bancolombia.com.co",
      "fechaInscripcion": "10-04-2019",
      "ultimaFechaModificacionMecanismo": "10-04-2019"
    },
    "status": "200"
  },
  "resDatosBasicosCliente": {
    "data": {
      "estadoCliente": "01",
      "codigoRol": "02",
      "informacionExpedicionIdentificacion": {
        "fechaExpedicion": "01-01-2001",
        "codigoPaisExpedicion": "CO",
        "codigoCiudadExpedicion": "0000001"
      },
      "llaveCIF": "190301082428003",
      "codigoSubSector": "114",
      "fechaEstadoVinculacion": "1551416400000",
      "estadoVinculacion": "03",
      "esClienteDeTesoreria": false,
      "identificacionCliente": {
        "tipoDocumento": "FS001",
        "numeroDocumento": "25130542"
      },
      "codigoOficina": "00000750",
      "cliente": {
        "personaNatural": {
          "nombreLargoCliente": "DANIEL JALISCO JIMENEZ",
          "nombreCliente": {
            "segundoNombre": " ",
            "primerApellido": "JALISCO",
            "primerNombre": "DANIEL",
            "segundoApellido": "JIMENEZ"
          },
          "nombreCortoCliente": "DANIEL JALISCO"
        }
      },
      "codigoSector": "10",
      "llaveMDM": "5643895",
      "fechaCambioEstado": "-62135406000000",
      "gerenteComercial": {
        "nombreLargo": "JESUS SALVADOR SALAZAR BERNAL"
      },
      "identificadorClienteBP": "0013976458",
      "informacionNacimientoCliente": {
        "fecha": "10-09-1981",
        "codigoCiudad": "0000001",
        "codigoPais": "CO"
      },
      "codigoSubCIIU": "000000",
      "codigoSegmento": "01",
      "codigoCIIU": "00010"
    },
    "status": "OK"
  },
  "resDatosPersonales": {
    "data": {
      "codigoIdioma": {
        "codigoISO6391": "ES"
      },
      "esMenorDeEdad": false,
      "esPEP": false,
      "actividadEconomica": "007",
      "codigoAutorizacionRecibirInformacion": {
        "viaMensajeTexto": false,
        "viaCorreoElectronico": false
      },
      "estadoVinculacion": "03",
      "identificacionCliente": {
        "tipoDocumento": "FS001",
        "numeroDocumento": "25130542"
      },
      "numeroHijos": 0,
      "genero": "M",
      "codigoPaisNacionalidad": {
        "codigoISO3166Alfa2": "CO"
      },
      "codigoEstadoCivil": "1",
      "tratamientoCliente": "0002",
      "empresaDondeLabora": " "
    },
    "status": "OK"
  },
  "reqTipoCliente": {
    "informacionDispositivo": {
      "deviceBrowser": "Chrome",
      "sistemaOperativo": "Windows",
      "dispositivo": "Unknown",
      "versionSistemaOperativo": " Windows NT 10.0; Win64; x64",
      "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36"
    },
    "idPlan": "10",
    "idAplicacion": "otrosPlanes",
    "idSesion": "369574501169-mF2YS8xLjguMF8366318670576",
    "datosCliente": {
      "tipoIdentificacion": "FS001",
      "correo": "ma@mal.com",
      "celular": "3147922277",
      "identificacion": "25130542"
    },
    "jwtToken": "03AOLTBLTC5RoFrDB7R6OSt87lcd5Gb_CpI7eyvv3Gee-HEqy2LM0mpFn71Xnb0BUi3QS42oSH9Yo4FDQyI6BHbxnTiUF7mGKFVGtQYupNuqRjJOJzRX2uajPw8uP5o1GPQg9fNLx2LzQVaLu87maJJNXAf-3scfMrcrOXSeKD3iZB3F1bChy8km3jWVP13uMcz4lVYgGTL8QmJeSY818Z57aga7RjcoNsA5Mon0M-AzO2UlYTLGx6O2A8FX4WdGgiWlmNIwOtofyV2jXwWacLwqMLAKKH0sMdI44Z65Tno0YwXGB37hRK7ePsHAi5aTLZ6V4E7d0r9xATUiZG1GruV5AsrIgl4u4DhA",
    "producto": {
      "codigoProducto": ""
    }
  },
  "resConsultaListaControl": {
    "data": {
      "consultarEstadoClienteListasControlResponse": {
        "tipoDocumento": "FS001",
        "tipoEstado": "6",
        "numeroDocumento": "25130542",
        "nombreCompleto": "DANIEL   JALISCO JIMENEZ",
        "mensajeEstado": "Cliente no esta en listas de control"
      }
    },
    "status": "200"
  }
};

const convenio = {
  request: null,
  response: null,
  errors: null,
  intento: 1,
  fechaHoraTransaccion: "2019-12-12", 
  esSatisfactorio: true,
  status: 200,
}

const proxyConvenio = {
  "status": "OK",
  "fault": null,
  "data": {
    "codigoConvenio": 1,
    "nitEmpleador": 8003458,
    "numeroPlan": 10,
    "grupoCobro": 3,
    "cuotaManejo": 120000
  }
}

const infoCliente = {
	"primerNombre":"",
	"segundoNombre":"",
	"primerApellido":"",
	"segundoApellido":"",
	"tipoIdentificacion":"FS001",
	"identificacion":"32456098",
	"fechaNacimiento":"",
	"genero":"",
	"estadoCivil":"",
	"celular":"",
	"correo":"",
	"esSatisfactorio":true,
	"CodError":"",
	"descripcionError":"",
	"tipoError":"",
	"idSesion":"123",
	"ocupacion":""
}

const recursos = {
  "status": 0,
  "catalogos": null,
  "variables": {
      "variables": [
          {
              "id": "ENDPOINT_API_PERSISTENCIA_RECUPERAR_DATOS_BY_SESSION_STEP",
              "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.commons.api.persistencia/api/persistence/tcl/findBySessionStep"
          },
          {
              "id": "ENDPOINT_API_PERSISTENCIA_GUARDAR_DATOS",
              "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.commons.api.persistencia/api/persistence/yc/save"
          },
          {
              "id": "API_TIPO_CLIENTE_DIAS_MODIFICACION_DATOS",
              "valor": "180"
          },
          {
              "id": "ENDPOINT_API_ERRORES",
              "valor": "https:cuentqaabrira.grupobancolombia.com/com.bancolombia.commons.api.errores/rest/servicio/errorbycode"
          },
          {
              "id": "ENDPOINT_API_PERSISTENCIA_RECUPERAR_DATOS_BY_NUMDOC",
              "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.commons.api.persistencia/api/persistence/tcl/findSessionByNumdoc"
          },
          {
              "id": "ENDPOINT_API_GESTION_AUTENTICACION_OAUTH2",
              "valor": "http://localhost:3000/session/token/oauth/token"
          },
          {
              "id": "CLIENT_ID_API_GESTION_AUTENTICACION_OAUTH2",
              "valor": "YCL"
          },
          {
              "id": "GRANT_TYPE_API_GESTION_AUTENTICACION_OAUTH2",
              "valor": "authorization_code"
          },
          {
              "id": "REDIRECT_URI_API_GESTION_AUTENTICACION_OAUTH2",
              "valor": "https://qaabrircuenta.grupobancolombia.com/bancolombia.dtd.b1c.yaCliente/home"
          },
          {
              "id": "PASO_FUNCIONAL_DATA_TIPO_CLIENTE",
              "valor": "TCL_VALIDATION"
          },
          {
              "id": "PASO_FUNCIONAL_DATA_VALIDA_SESION",
              "valor": "YCL_VSESION"
          }
      ]
  },
  "mensajesError": null,
  "error": null
}

const codigoOauth = {
	"document_number":"111",
  "last_entry_date":"2019-10-10"
}

const responsePersistencia = {
  "status": 200,
  "dateTime": "2019-05-21 09:10:19",
  "data": "73256403927-mF2YS8xLjguMF8404227988616"
}

const recursosPlan = {
  "catalogos": null,
  "variables": 
  {
    "variables":
  [
      {
          "id": "PROXY_SYSTEM_ID_GENERACION_TOKEN",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_USERNAME_GENERACION_TOKEN",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_CLASSIFICATION_GENERACION_TOKEN",
          "valor": "http://grupobancolombia.com/clas/AplicacionesActuales#"
      },
      {
          "id": "PROXY_REQUEST_TIMEOUT_GENERACION_TOKEN",
          "valor": "16"
      },
      {
          "id": "PROXY_ID_SERVIDOR_GENERACION_TOKEN",
          "valor": "OTP"
      },
      {
          "id": "PROXY_CONNECTION_TIMEOUT_GENERACION_TOKEN",
          "valor": "16"
      },
      {
          "id": "PROXY_ID_SISTEMA_FUENTE_GENERACION_TOKEN",
          "valor": "VIN"
      },
      {
          "id": "PROXY_ENDPOINT_GENERACION_TOKEN",
          "valor": "https://serviciosdpdyc.bancolombia.com:59065/intf/ServiciosDeDesarrollo/Integracion/ChannelAdapterDP/Enlace/V1.0"
      },
      {
          "id": "PROXY_GENERACION_TOKEN_NAMESPACE",
          "valor": "http://grupobancolombia.com/intf/Canal/Movil/GeneracionToken/V1.0"
      },
      {
          "id": "PROXY_SYSTEM_ID_CONTRIBUCION_DOCUMENTAL",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_USERNAME_CONTRIBUCION_DOCUMENTAL",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_REQUEST_TIMEOUT_CONTRIBUCION_DOCUMENTAL",
          "valor": "16"
      },
      {
          "id": "PROXY_CONNECTION_TIMEOUT_CONTRIBUCION_DOCUMENTAL",
          "valor": "16"
      },
      {
          "id": "PROXY_ENDPOINT_CONTRIBUCION_DOCUMENTAL",
          "valor": "https://serviciosdpdyc.bancolombia.com:59058/intf/Corporativo/AdministracionDocumentos/ContribucionDocumental/V1.0"
      },
      {
          "id": "PROXY_NAMESPACE_CONTRIBUCION_DOCUMENTAL",
          "valor": "http://grupobancolombia.com/intf/Corporativo/AdministracionDocumentos/ContribucionDocumental/V1.0"
      },
      {
          "id": "PROXY_CLASSIFICATION_CONTRIBUCION_DOCUMENTAL",
          "valor": "http://grupobancolombia.com/clas/AplicacionesActuales#"
      },
      {
          "id": "URL_CONTRIBUCION_DOCUMENTAL",
          "valor": "https://qaabrircuenta.grupobancolombia.com/APIPROXY.contribucionDocumental/rest/servicio/contribuirDocumentoConFirma"
      },
      {
          "id": "NOMBRE_ARCHIVO_ADJUNTO",
          "valor": "log.txt"
      },
      {
          "id": "FWK_ETIQUETA_ARCHIVO_ADJUNTO",
          "valor": "trazabilidad.txt"
      },
      {
          "id": "API_PDF_ACUERDO_YCL",
          "valor": "/b1c/wrkdirr/mail_cuenta_nomina_adultos"
      },
      {
          "id": "FWK_NOMBRE_DOCUMENTO_VINCULACION_FIRMA_DIGITAL",
          "valor": "F-1460-FORMATO-VINCULACION-DIGITAL-SIN-ADVERTENCIA.pdf"
      },
      {
          "id": "FWK_TIPO_DOCUMENTO_CEDULA_CIUDADANIA_LEGADO",
          "valor": "FS001"
      },
      {
          "id": "FWK_CODIGO_SUBTIPO_DOCUMENTAL",
          "valor": "1470"
      },
      {
          "id": "FWK_ETIQUETA_FIRMA_ELECTRONICA",
          "valor": "FirmaCliente"
      },
      {
          "id": "FWK_TEXTO_HABEAS_DATA",
          "valor": "Autorizo a Bancolombia para que el nÃºmero de celular y el correo electrÃ³nico, sean tratados para contactarme y/o enviarme la informaciÃ³n relacionada con la apertura de la cuenta. Igualmente para que me consulten ante Operadores de InformaciÃ³n y Riesgo con el fin de verificar mi informaciÃ³n personal."
      },
      {
          "id": "FWK_PREFIJO_NOMBRE_ARCHIVO_PDF",
          "valor": "FormularioConocimientoCliente_"
      },
      {
          "id": "FWK_MENSAJE_ERROR_GENERICO",
          "valor": "En este momento el sistema no estÃ¡ disponible. Por favor intenta mÃ¡s tarde."
      },
      {
          "id": "SHOW_XML_SOAP_BODY",
          "valor": "true"
      },
      {
          "id": "PROXY_SYSTEM_ID_GENERAR_OTP",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_USERNAME_GENERAR_OTP",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_CLASSIFICATION_GENERAR_OTP",
          "valor": "http://grupobancolombia.com/clas/AplicacionesActuales#"
      },
      {
          "id": "PROXY_REQUEST_TIMEOUT_GENERAR_OTP",
          "valor": "32"
      },
      {
          "id": "PROXY_CONNECTION_TIMEOUT_GENERAR_OTP",
          "valor": "32"
      },
      {
          "id": "PROXY_ENDPOINT_GENERAR_OTP",
          "valor": "https://serviciosdpdyc.bancolombia.com:59065/intf/ServiciosDeDesarrollo/Integracion/ChannelAdapterDP/Enlace/V1.0"
      },
      {
          "id": "PROXY_NAMESPACE_GENERAR_OTP",
          "valor": "http://grupobancolombia.com/intf/SeguridadCorporativa/Canales/GenerarOTP/V2.0"
      },
      {
          "id": "PROXY_AUTENTICAR_CLIENTE_OTP_SYSTEM_ID",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_AUTENTICAR_CLIENTE_OTP_USERNAME",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_AUTENTICAR_CLIENTE_OTP_CLASSIFICATION",
          "valor": "http://grupobancolombia.com/clas/AplicacionesActuales#"
      },
      {
          "id": "PROXY_AUTENTICAR_CLIENTE_OTP_REQUEST_TIMEOUT",
          "valor": "16"
      },
      {
          "id": "PROXY_AUTENTICAR_CLIENTE_OTP_CONNECTION_TIMEOUT",
          "valor": "16"
      },
      {
          "id": "PROXY_AUTENTICAR_CLIENTE_OTP_ENDPOINT",
          "valor": "https://serviciosdpdyc.bancolombia.com:59065/intf/ServiciosDeDesarrollo/Integracion/ChannelAdapterDP/Enlace/V1.0"
      },
      {
          "id": "PROXY_AUTENTICAR_CLIENTE_OTP_NAMESPACE",
          "valor": "http://grupobancolombia.com/intf/SeguridadCorporativa/Canales/AutenticarClienteOTP/V2.0"
      },
      {
          "id": "PROXY_AUTENTICAR_CLIENTE_OTP_SHOW_XML_SOAP_BODY",
          "valor": "true"
      },
      {
          "id": "API_INFO_CONFIRMACION_PLAN_NUMERO_MAX_INTENTOS_TOKEN",
          "valor": "5"
      },
      {
          "id": "API_INFO_CONFIRMACION_PLAN_NUMERO_MAX_INTENTOS_SOFTTOKEN",
          "valor": "4"
      },
      {
          "id": "ENDPOINT_API_ERRORES",
          "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.commons.api.errores/rest/servicio/errorbycode"
      },
      {
          "id": "ENDPOINT_API_PERSISTENCIA",
          "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.commons.api.persistencia/api/persistence/yc/save"
      },
      {
          "id": "URL_APIPROXY_GENERACION_TOKEN",
          "valor": "https://qaabrircuenta.grupobancolombia.com/co.com.bancolombia.vd.api.proxy.generacionToken"
      },
      {
          "id": "ENDPOINT_APIPROXY_GENERACION_SOLICITAR_TOKEN",
          "valor": "/rest/servicio/solicitarToken"
      },
      {
          "id": "ENDPOINT_APIPROXY_GENERACION_VALIDAR_TOKEN",
          "valor": "/rest/servicio/validarToken"
      },
      {
          "id": "API_INFO_CONFIRMACION_PLAN_CODIGO_OPERACION",
          "valor": "032"
      },
      {
          "id": "API_INFO_CONFIRMACION_PLAN_CODIGO_APLICACION",
          "valor": "VENTASDIGI"
      },
      {
          "id": "API_INFO_CONFIRMACION_PLAN_ASUNTO_INFORMACION_MENSAJE",
          "valor": "Servicio de Alertas y Notificaciones Bancolombia"
      },
      {
          "id": "API_INFO_CONFIRMACION_PLAN_CONTENIDO_MENSAJE",
          "valor": "Nueva clave dinamica"
      },
      {
          "id": "URL_AUTENTICAR_CLIENTE_ODA",
          "valor": "https://qaabrircuenta.grupobancolombia.com/APIPROXY.autenticarClienteOTP/rest/servicio/autenticarClienteOTPODA"
      },
      {
          "id": "URL_GENERAR_ENVIAR_TOKEN",
          "valor": "https://qaabrircuenta.grupobancolombia.com/APIPROXY.generarOTPODA/rest/servicio/generarEnviarOTPODA"
      },
      {
          "id": "VALIDAR_TOKEN_SUCESS",
          "valor": "901"
      },
      {
          "id": "URL_REDIRECCION_COMPARADOR",
          "valor": "https://www.grupobancolombia.com/wps/portal/personas/solicitud-de-productos-wcm#/tarjetaCredito"
      },
      {
          "id": "DATOS_CLIENTE_CDO_DIRECCION_PRINCIPAL",
          "valor": "Z001"
      },
      {
          "id": "ENDPOINT_API_PERSISTENCIA_RECUPERAR_DATOS_TCL",
          "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.clientes.api.persistencia/api/persistence/tcl/findBySessionStep"
      },
      {
          "id": "ENDPOINT_API_PERSISTENCIA_RECUPERAR_DATOS_YC",
          "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.clientes.api.persistencia/api/persistence/yc/findBySessionStep"
      },
      {
          "id": "ENDPOINT_API_PERSISTENCIA_GUARDAR_DATOS_YC",
          "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.commons.api.persistencia/api/persistence/yc/save"
      },
      {
          "id": "ENDPOINT_API_PROXY_GESTION_CUENTA_CREAR_CUENTA",
          "valor": "https://qaabrircuenta.grupobancolombia.com/APIPROXY.gestionDeCuentas/rest/servicio/crearCuenta"
      },
      {
          "id": "GESTION_DE_CUENTAS_TIPO_CUENTA_DEFECTO",
          "valor": "7"
      },
      {
          "id": "GESTION_DE_CUENTAS_COD_PLAN_NOMINA",
          "valor": "13"
      },
      {
          "id": "GESTION_DE_CUENTAS_INDICADOR_CONVENIO_TRADICIONAL",
          "valor": "N"
      },
      {
          "id": "GESTION_DE_CUENTAS_INDICADOR_CONVENIO_NOMINA",
          "valor": "S"
      },
      {
          "id": "GESTION_DE_CUENTAS_OFICINA_VENTA",
          "valor": "912"
      },
      {
          "id": "GESTION_DE_CUENTAS_OFICINA_VENTA_DUENA",
          "valor": "912"
      },
      {
          "id": "GESTION_DE_CUENTAS_CODIGO_VENDEDOR",
          "valor": "912"
      },
      {
          "id": "PROXY_SYSTEM_ID_GESTION_DE_CUENTAS",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_USERNAME_GESTION_DE_CUENTAS",
          "valor": "AW1128"
      },
      {
          "id": "PROXY_CLASSIFICATION_GESTION_DE_CUENTAS",
          "valor": "http://grupobancolombia.com/clas/AplicacionesActuales#"
      },
      {
          "id": "PROXY_CONNECTION_TIMEOUT_GESTION_DE_CUENTAS",
          "valor": "16"
      },
      {
          "id": "PROXY_REQUEST_TIMEOUT_GESTION_DE_CUENTAS",
          "valor": "16"
      },
      {
          "id": "PROXY_GESTION_DE_CUENTAS_TIPO_GARANTIA",
          "valor": "16"
      },
      {
          "id": "PROXY_ENDPOINT_GESTION_DE_CUENTAS",
          "valor": "https://serviciosdpdyc.bancolombia.com:59065/intf/ServiciosDeDesarrollo/Integracion/ChannelAdapterDP/Enlace/V1.0"
      },
      {
          "id": "PROXY_GESTION_DE_CUENTAS_NAMESPACE",
          "valor": "http://grupobancolombia.com/intf/Producto/CuentaAhorroCorriente/GestionDeCuentas/V1.1"
      },
      {
          "id": "PROXY_GESTION_DE_CUENTAS_SHOW_XML_SOAP_BODY",
          "valor": "true"
      },
      {
          "id": "ENDPOINT_API_PERSISTENCIA_RECUPERAR_DATOS_BY_SESSION_STEP",
          "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.commons.api.persistencia/api/persistence/tcl/findBySessionStep"
      },
      {
          "id": "URL_CONSUMIR_PDF",
          "valor": "http://localhost:8081/api/generarpdf/generate"
      },
      
      {
        "id": "API_RUTA_PLANTILLA_CORREO_VINCULACION",
        "valor": "/home/jefferson/Documents/IBM/Bancolombia/wrkdirr/mail_cuenta_nomina_adultos"
      },
      {
        "id": "API_NOMBRE_PLANTILLA_CORREO_VINCULACION_OTRAS_CUENTAS",
        "valor": "Email-cuentaAhorros.vm"
      },
      {
        "id": "FWK_USUARIO_CORREO",
        "valor": "support@b1c.bancolombia.com"
      },
        {
        "id": "FWK_CLAVE_CORREO",
        "valor": "IbmB1C2016"
      },
      {
        "id": "API_URL_REFERENCIA_BANCARIA",
        "valor": "http://localhost:9080/bancolombia.dtd.b1c.api.referenciaBancaria/rest/servicio/referenciaBancaria"
      },
      {
        "id": "FWK_NOMBRE_ADJUNTO_VINCULACION_CORREO",
        "valor": "DocumentoVinculacion.pdf"
      },
      {
        "id": "FWK_NOMBRE_ADJUNTO_REFERENCIA_CORREO",
        "valor": "ReferenciaBancaria.pdf"
      },
        {
        "id": "API_ASUNTO_CORREO_VINCULACION",
        "valor": "Vinculacion"
      },
        {
        "id": "API_DE_CORREO_VINCULACION",
        "valor": "no-reply@bancolombia.com"
      },
      {
        "id": "PROXY_GESTION_DE_CUENTAS_COD_MSJ_NO_EXONERA_CUENTA",
        "valor": "0100"
      },
      {
        "id": "PROXY_GESTION_DE_CUENTAS_COD_MSJ_NO_EXONERA_BANCO_CUENTA",
        "valor": "0746"
      },
      {
        "id": "PROXY_GESTION_DE_CUENTAS_MSJ_NO_EXONERA_CUENTA",
        "valor": "itar la exoneración."
      },
      {
        "id": "PROXY_GESTION_DE_CUENTAS_MSJ_NO_EXONERA_BANCO_CUENTA",
        "valor": "itar el retiro de la exoneración con esta entidad y luego solicitarla en tu nueva cuenta por la Sucursal Telefónica Bancolombia 01 800 0912345."
      },
      {
        "id": "PROXY_GESTION_DE_CUENTAS_MSJ_EXONERA_CUENTA",
        "valor": "dddddng"
      }
  ]
  },
  "mensajesError": null
};

const recursosTipoCliente = {
  "catalogos": null,
  "variables": 
  {
    "variables":
  [
    {
      "id": "SHOW_XML_SOAP_BODY",
      "valor": "true"
    },
    {
      "id": "PROXY_SYSTEM_ID_GESTION_AUTENTICACION_FUERTE",
      "valor": "AW1128"
    },
    {
      "id": "PROXY_USERNAME_GESTION_AUTENTICACION_FUERTE",
      "valor": "AW1128"
    },
    {
      "id": "PROXY_CLASSIFICATION_GESTION_AUTENTICACION_FUERTE",
      "valor": "http://grupobancolombia.com/clas/AplicacionesActuales#"
    },
    {
      "id": "PROXY_REQUEST_TIMEOUT_GESTION_AUTENTICACION_FUERTE",
      "valor": "16"
    },
    {
      "id": "PROXY_CONNECTION_TIMEOUT_GESTION_AUTENTICACION_FUERTE",
      "valor": "16"
    },
    {
      "id": "PROXY_ENDPOINT_GESTION_AUTENTICACION_FUERTE",
      "valor": "https://serviciosdpdyc.bancolombia.com:59065/intf/ServiciosDeDesarrollo/Integracion/ChannelAdapterDP/Enlace/V1.0"
    },
    {
      "id": "PROXY_NAMESPACE_GESTION_AUTENTICACION_FUERTE",
      "valor": "http://grupobancolombia.com/intf/Seguridad/Autenticacion/GestionAutenticacionFuerte/Enlace/V1.0"
    },
    {
      "id": "PROXY_GESTION_AUTENTICACION_FUERTE_COD_RESPUESTA_EXITOSA",
      "valor": "Creado"
    },
    {
      "id": "PROXY_SYSTEM_ID_RECUPERAR_DATOS_CLIENTE",
      "valor": "AW1128"
    },
    {
      "id": "PROXY_USERNAME_RECUPERAR_DATOS_CLIENTE",
      "valor": "AW1128"
    },
    {
      "id": "PROXY_CLASSIFICATION_RECUPERAR_DATOS_CLIENTE",
      "valor": "http://grupobancolombia.com/clas/AplicacionesActuales#"
    },
    {
      "id": "PROXY_REQUEST_TIMEOUT_RECUPERAR_DATOS_CLIENTE",
      "valor": "16"
    },
    {
      "id": "PROXY_CONNECTION_TIMEOUT_RECUPERAR_DATOS_CLIENTE",
      "valor": "16"
    },
    {
      "id": "PROXY_ENDPOINT_RECUPERAR_DATOS_CLIENTE",
      "valor": "https://serviciosdpdyc.bancolombia.com:59065/intf/ServiciosDeDesarrollo/Integracion/ChannelAdapterDP/Enlace/V1.0"
    },
    {
      "id": "PROXY_RECUPERAR_DATOS_CLIENTE_NAMESPACE",
      "valor": "http://grupobancolombia.com/intf/Clientes/GestionClientes/RecuperarDatosCliente/V4.0"
    },
    {
      "id": "ENDPOINT_API_PROXY_DATOS_BASICOS_CLIENTE",
      "valor": "https://qaabrircuenta.grupobancolombia.com/APIPROXY.recuperarDatosClientesMDM/rest/servicio/recuperarDatosBasicosCliente"
    },
    {
      "id": "ENDPOINT_API_PROXY_DATOS_PERSONALES_CLIENTE",
      "valor": "https://qaabrircuenta.grupobancolombia.com/APIPROXY.recuperarDatosClientesMDM/rest/servicio/recuperarDatosPersonalesCliente"
    },
    {
      "id": "ENDPOINT_API_PROXY_DATOS_UBICACION_CLIENTE",
      "valor": "https://qaabrircuenta.grupobancolombia.com/APIPROXY.recuperarDatosClientesMDM/rest/servicio/recuperarDatosUbicacionCliente"
    },
    {
      "id": "ENDPOINT_API_PROXY_DATOS_FINANCIEROS_CLIENTE",
      "valor": "https://qaabrircuenta.grupobancolombia.com/APIPROXY.recuperarDatosClientesMDM/rest/servicio/recuperarDatosFinancierosCliente"
    },
    {
      "id": "ENDPOINT_API_PROXY_GESTION_AUTH_FUERTE",
      "valor": "https://qaabrircuenta.grupobancolombia.com/APIPROXY.gestionAuthFuerte/rest/servicio/consultarEnrolamientoCliente"
    },
    {
      "id": "ENDPOINT_API_PROXY_CONSULTA_ESTADO_CLIENTE_LISTAS_CONTROL",
      "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.commons.api.proxy.consultaEstadoClienteListasControl/rest/servicio/consultarEstadoClienteListasControl"
    },
    {
      "id": "ENDPOINT_API_ERRORES",
      "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.commons.api.errores/rest/servicio/errorbycode"
    },
    {
      "id": "ENDPOINT_API_PERSISTENCIA_GUARDAR_DATOS",
      "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.clientes.api.persistencia/api/persistence/tcl/save"
    },
    {
      "id": "ENDPOINT_API_PERSISTENCIA_RECUPERAR_DATOS",
      "valor": "https://qaabrircuenta.grupobancolombia.com/com.bancolombia.clientes.api.persistencia/api/persistence/tcl/findBySessionStep"
    },
    {
      "id": "API_TIPO_CLIENTE_DIAS_MODIFICACION_DATOS",
      "valor": "5"
    },
    {
      "id": "COD_ROL_CLIENTE_ACTUAL",
      "valor": "02"
    },
    {
      "id": "API_TIPO_CLIENTE_DIAS_MODIFICACION_MECANISMO",
      "valor": "5"
    },
    {
      "id": "API_TIPO_CLIENTE_DIAS_CREACION_MECANISMO",
      "valor": "5"
    },
    {
      "id": "API_TIPO_CLIENTE_EDAD_MINIMA_REQUERIDA",
      "valor": "14"
    },
    {
      "id": "URL_REDIRECCION_OAUTH",
      "valor": "https://autenticacion.grupobancolombia.com/login/oauth/authorize?response_type=code&client_id=YCL&redirect_uri=https://abrircuenta.grupobancolombia.com/bancolombia.dtd.b1c.yaCliente/inicio.html"
    },
    {
      "id": "URL_REDIRECCION_COMPARADOR",
      "valor": "https://www.grupobancolombia.com/wps/portal/personas/solicitud-de-productos-wcm"
    },
    {
      "id": "URL_REDIRECCION_NO_CLIENTE_NOMINA",
      "valor": "https://abrircuenta.grupobancolombia.com/bancolombia.dtd.b1c.vinculacion/portal.html"
    },
    {
      "id": "URL_REDIRECCION_NO_CLIENTE_PLAN10",
      "valor": "https://abrircuenta.grupobancolombia.com/bancolombia.dtd.b1c.vinculacionOtras/planEstandar.html"
    },
    {
      "id": "URL_REDIRECCION_NO_CLIENTE_PLAN12",
      "valor": "https://abrircuenta.grupobancolombia.com/bancolombia.dtd.b1c.vinculacionOtras/planBasico.html"
    },
    {
      "id": "URL_REDIRECCION_NO_CLIENTE_PLAN31",
      "valor": "https://abrircuenta.grupobancolombia.com/bancolombia.dtd.b1c.vinculacionOtras/planPremium.html"
    },
    {
      "id": "DATOS_CLIENTE_CDO_DIRECCION_PRINCIPAL",
      "valor": "Z001"
    },
    {
      "id": "ID_PLAN_NOMINA",
      "valor": "13"
    },
    {
      "id": "PROXY_SYSTEM_ID_CONSULTA_ESTADO_CLIENTE_LISTAS_CONTROL",
      "valor": "AW1128"
    },
    {
      "id": "PROXY_USERNAME_CONSULTA_ESTADO_CLIENTE_LISTAS_CONTROL",
      "valor": "AW1128"
    },
    {
      "id": "PROXY_CLASSIFICATION_CONSULTA_ESTADO_CLIENTE_LISTAS_CONTROL",
      "valor": "http://grupobancolombia.com/clas/AplicacionesActuales#"
    },
    {
      "id": "PROXY_REQUEST_TIMEOUT_CONSULTA_ESTADO_CLIENTE_LISTAS_CONTROL",
      "valor": "16"
    },
    {
      "id": "PROXY_CONNECTION_TIMEOUT_CONSULTA_ESTADO_CLIENTE_LISTAS_CONTROL",
      "valor": "16"
    },
    {
      "id": "PROXY_ENDPOINT_CONSULTA_ESTADO_CLIENTE_LISTAS_CONTROL",
      "valor": "https://serviciosdpdyc.bancolombia.com:59065/intf/ServiciosDeDesarrollo/Integracion/ChannelAdapterDP/Enlace/V1.0"
    },
    {
      "id": "PROXY_CONSULTA_ESTADO_CLIENTE_LISTAS_CONTROL_NAMESPACE",
      "valor": "http://grupobancolombia.com/intf/ServiciosApoyo/PrevencionLavadoActivos/ConsultarEstadoClienteListasControl/V1.0"
    },
    {
      "id": "PROXY_CODIGO_CALIFICACION_LISTA_CONTROL",
      "valor": "6"
    },
    {
      "id": "API_CAPTCHA_VALIDATE_ENDPOINT",
      "valor": "http://localhost:3000/com.bancolombia.commons.api.captcha/api/captcha/validarcaptcha"
    },
    {
      "id": "PROXY_LLAVE_CANAL",
      "valor": "codigoCanal"
    },
    {
      "id": "PROXY_VALOR_CANAL",
      "valor": "VPI"
    },
    {
      "id": "PROXY_LLAVE_IPCLIENTE",
      "valor": "ipCliente"
    },
    {
      "id": "FWK_CATALOG_PATH",
      "valor": "/b1c/wrkdirr/catalogos/"
    },
    {
      "id": "FWK_CATALOGOS_APLICACION",
      "valor": "FWK_CATALOGO_TIPODOCVPI"
    },
    {
      "id": "FWK_CATALOGO_TIPODOCVPI",
      "valor": "CATALOGO_TIPODOCVPI"
    },
    {
      "id": "FWK_TIPODOC_TARJETA_IDENTIDAD",
      "valor": "FS004"
    }
  ]
  },
  "mensajesError": null
};

const captcha = {
  estadoValidacion:"true",
  publickey:"123456",
  operacion:"captcha"
};

module.exports = {
    fault,
    authFuerte,
    listasControl,
    datosCliente,
    datosUbicacion,
    recursos,
    errores,
    tipoCliente,
    convenio,
    proxyConvenio,
    infoCliente,
    codigoOauth,
    responsePersistencia,
    recursosPlan,
    recursosTipoCliente
};





