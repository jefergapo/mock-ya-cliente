const router = require('express').Router();
const mocks = require('./mocks');

// Use middleware to set the default Content-Type


router.post('/com.bancolombia.commons.api.proxy.gestionListasDeControl/rest/servicio/registrarClienteListasDeControl', (req, res, next) => {
    console.log( "[CONSULTA LISTAS DE CONTROL] payload: ", req.body.payload );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.send( mocks.listasControl );
});

router.post('/com.bancolombia.commons.api.proxy.recuperarDatosClientesMDM/rest/servicio/recuperarDatosBasicosCliente', (req, res, next) => {
    console.log( "[CONSULTA DATOS BASICOS] payload: ", req.body.payload );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.send( mocks.datosCliente );
});

router.post('/com.bancolombia.commons.api.proxy.recuperarDatosClientesMDM/rest/servicio/recuperarDatosUbicacionCliente', (req, res, next) => {
    console.log( "[CONSULTA DATOS UBICACION] payload: ", req.body.payload );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.send( mocks.datosUbicacion );
});

router.post('/APIPROXY.gestionauthfuerte/rest/servicio/consultarEnrolamientoCliente', (req, res, next) => {
    console.log( "[CONSULTA AUTH FUERTE] payload: ", req.body.payload );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.send( mocks.authFuerte );
});

router.post('/recursos', (req, res, next) => {
    console.log( "[CONSULTA RECURSOS] payload: ", req.body );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.send( mocks.recursos );
});

router.post('/errores', (req, res, next) => {
    console.log( "[CONSULTA ERRORES] payload: ", req.body.payload );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.send( mocks.errores );
});

router.post('/persistencia', (req, res, next) => {
    console.log( "[PERSISTENCIA-GUARDAR] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(400).send( { "status":"200", "dateTime":"12-12-12" ,"data": JSON.stringify( mocks.infoCliente) } );
});

router.post('/getInfoTipoCliente/:idSession', (req, res, next) => {
    console.log( "[PERSISTENCIA-GUARDAR] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(200).send( mocks.tipoCliente );
});


router.post('/persistencia/recuperar-data', (req, res, next) => {
    console.log( "[PERSISTENCIA-RECUPERAR] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(200).send( { "status":"200", "dateTime":"12-12-12" ,"data": null } );
});

router.post('/rest/servicio/consultarPlanesNomina', (req, res, next) => {
    console.log( "[CONSULTARPLANESNOMINA] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(200).send( mocks.proxyConvenio );
});


router.post('/session/recursos', (req, res, next) => {
    console.log( "[Recursos] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(200).send( mocks.recursos );
});

router.post('/session/com.bancolombia.commons.api.persistencia/api/persistence/tcl/findSessionByNumdoc', (req, res, next) => {
    console.log( "[code-doc] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(200).send( mocks.responsePersistencia );
});

router.post('/session/token/oauth/token', (req, res, next) => {
    console.log( "[Oauth] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(200).send( mocks.codigoOauth );
});

router.post('/recursos/plan', (req, res, next) => {
    console.log( "[RECURSOS_PLAN] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(200).send( mocks.recursosPlan );
});

router.post('/recursos/tipo-cliente', (req, res, next) => {
    console.log( "[RECURSOS_PLAN] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(200).send( mocks.recursosTipoCliente );
});

router.post('/com.bancolombia.commons.api.captcha/api/captcha/validarcaptcha', (req, res, next) => {
    console.log( "[RECURSOS_PLAN] payload: ",  JSON.stringify( req.body ) );
    res.set({'Content-Type': 'application/json;charset=UTF-8'});
    res.status(200).send( mocks.recursosTipoCliente );
});

module.exports = router;

